[简体中文](README_zh-cn.md)丨[繁体中文](README_zh-tw.md)丨English

<div align="center">

   <img src=".github/logo.svg" alt="Solitude logo" height="200">

  <h1 align="center">Hexo Theme Solitude</h1>

  <p align="center">An elegant Hexo theme that supports lazy loading, PWA, Latex, and multiple comment systems.</p>

![release](https://img.shields.io/github/package-json/v/valor-x/hexo-theme-solitude/master?color=%231ab1ad&label=release)
![https://img.shields.io/npm/v/hexo-theme-solitude?color=%09%23bf00ff](https://img.shields.io/npm/v/hexo-theme-solitude?color=%09%23bf00ff)
![license](https://img.shields.io/github/license/valor-x/hexo-theme-solitude?color=FF5531)
![hexo version](https://img.shields.io/badge/hexo-6.3.0+-0e83c)
[![Stars](https://img.shields.io/github/stars/valor-x/hexo-theme-solitude)](https://github.com/valor-x/hexo-theme-solitude/stargazers)
[![GitHub all releases](https://img.shields.io/github/downloads/valor-x/hexo-theme-solitude/total)](https://github.com/valor-x/hexo-theme-solitude/releases/latest)

[Preview](https://solitude-demo.efu.me/) 丨  [Documentation](https://solitude-docs.efu.me/)

</div>

![Screenshot](.github/screenshot.png)

## Features

- Pjax、LazyLoad、PWA
- Comments(Twikoo、Waline)
- ColorMode
- Lightbox(medium-zoom、fancybox)
- Mathematical formulas(Latex)
- Featured pages: Instant Short Article, My Equipment, Online Tools, Music Gallery, Friend Chain Fish Pond, Photo Album Page, Douban Page
- Article features: AI summary, code highlighting

## Todo

- [x] Support Waline
- [ ] Tags plugin
- [ ] Album reconstruction(Removed, to be refactored)

## Setup

1. Use the NPM package
      ```bash
      npm i hexo-theme-solitude
      ```
2. Apply
      ```yaml
      theme: solitude
      ```

Check out the [Documentation](https://solitude-docs.efu.me/) for more information.

## Contributors

<a href="https://github.com/valor-x/hexo-theme-solitude/graphs/contributors">
  <img alt="contributors" src="https://contrib.rocks/image?repo=valor-x/hexo-theme-solitude" />
</a>

**Theme is fully licensed and designed by the author of [@张洪Heo](https://github.com/zhheo)!**

> You are welcome to contribute to this topic! [Contribution Guide](/CONTRIBUTING.md)