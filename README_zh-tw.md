[简体中文](README_zh-cn.md)丨繁体中文丨[English](README.md)

<div align="center">
  
   <img src=".github/logo.svg" alt="Solitude logo" height="200">

  <h1 align="center">Hexo Theme Solitude</h1>

  <p align="center">一款優雅的 Hexo 主題，支援懶載入、PWA、Latex以及多種評論系統。</p>

![release](https://img.shields.io/github/package-json/v/valor-x/hexo-theme-solitude/master?color=%231ab1ad&label=release)
![https://img.shields.io/npm/v/hexo-theme-solitude?color=%09%23bf00ff](https://img.shields.io/npm/v/hexo-theme-solitude?color=%09%23bf00ff)
![license](https://img.shields.io/github/license/valor-x/hexo-theme-solitude?color=FF5531)
![hexo version](https://img.shields.io/badge/hexo-6.3.0+-0e83c)
[![Stars](https://img.shields.io/github/stars/valor-x/hexo-theme-solitude)](https://github.com/valor-x/hexo-theme-solitude/stargazers)
[![GitHub all releases](https://img.shields.io/github/downloads/valor-x/hexo-theme-solitude/total)](https://github.com/valor-x/hexo-theme-solitude/releases/latest)

[預覽](https://solitude-demo.efu.me/) 丨  [文档](https://solitude-docs.efu.me/)

</div>

![Screenshot](.github/screenshot.png)

## 特性

- 頁面懶載入（Pjax）、圖片懶載入（LazyLoad）、離線應用（PWA） 
- 評論（Twikoo、Waline） 
- 顯示切換（ColorMode） 
- 燈箱（medium-zoom、fancybox） 
- 數學公式（Latex） 
- 特色頁面：即刻短文、我的裝備、在線工具、音樂館、友鏈魚塘、相冊頁、豆瓣頁 
- 文章功能：AI 摘要、代碼高亮

## Todo

- [x] Waline 評論支援
- [ ] Tags plugin 外掛程式化
- [ ] 相冊頁重構（已移除，待重構）

## 應用

1. 使用 NPM 包進行安裝
      ```bash
      npm i hexo-theme-solitude
      ```
2. 應用主題
      ```yaml
      theme: solitude
      ```

前往 [文檔](https://solitude-docs.efu.me) 獲取更多資訊。

## 貢獻者

<a href="https://github.com/valor-x/hexo-theme-solitude/graphs/contributors">
  <img alt="contributors" src="https://contrib.rocks/image?repo=valor-x/hexo-theme-solitude" />
</a>

**主題由 [@张洪Heo](https://github.com/zhheo) 全權授權、設計！**

> 歡迎你對本主題做出貢獻！[貢獻指南](/CONTRIBUTING.md)
